
//import { Fragment } from 'react';
import { useState, useEffect } from 'react';
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router } from 'react-router-dom';
import { Routes, Route } from 'react-router-dom';

//COMPONENTS
import AppNavbar from './components/AppNavbar';


//PAGES
import AdminDashboard from './pages/AdminDashboard';
import Home from './pages/Home';
import Error from './pages/Error404';
import Login from './pages/Login';
import Logout from './pages/Logout';
import NewProduct from './pages/NewProduct';
import Orders from './pages/Orders'
import Product from './pages/Products';
import ProductUpdate from './pages/ProductUpdate';
import ProductView from './pages/ProductView'
import Register from './pages/Register';

import './App.css';
import { UserProvider } from './UserContext';


function App() {


  const [user, setUser] = useState({
      id: null,
      isAdmin: null
  });

  const unsetUser = () => {
    localStorage.clear();
  }


  useEffect(() => {

        fetch(`http://localhost:8000/users/user.id`, {
            headers: {
                Authorization: `Bearer ${ localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {

            if (typeof data._id !== "undefined") {
                setUser({
                    id: data._id,
                    isAdmin: data.isAdmin
                })
            } else {
                setUser({
                    id: null,
                    isAdmin: null
                })
            }
        })

    }, []);



  return (
    <div className="App-header">
     <UserProvider value={{user, setUser, unsetUser}} >
        <Router>
          <AppNavbar />
          <Container>
            <Routes>
              <Route path="/" element={<Home/>} />
              <Route path="/dashboard" element={<AdminDashboard/>} />
              <Route path="/edit-products/:productId" element={<ProductUpdate/>} />
              <Route path="/products" element={<Product/>} />
              <Route path="/products/newProduct" element={<NewProduct/>} />
              <Route path="/products/:productId" element={<ProductView/>} />
              <Route path="/orders" element={<Orders />} />
              <Route path="/register" element={<Register/>} />
              <Route path="/login" element={<Login/>} />
              <Route path="/logout" element={<Logout/>} />
              <Route path="*" element={<Error/>} />
            </Routes>
          </Container>
        </Router>
      </UserProvider>
    </div>
  );
}

export default App;
