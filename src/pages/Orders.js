import { Fragment, useEffect, useState, useContext } from 'react';
import { Table, Button } from 'react-bootstrap';
import { Navigate, Link, useNavigate } from 'react-router-dom';
import UserOrder from '../components/UserOrder'
import Swal from "sweetalert2";
import UserContext from "../UserContext";



export default function Orders() {
	
	const {user} = useContext(UserContext);

	const navigate = useNavigate();

	const [orders, setOrders] = useState([]);

	
	const fetchProductData = () => {
		fetch(`${process.env.REACT_APP_API_URL}/orders/user`,{
				headers:{
					"Authorization": `Bearer ${localStorage.getItem("token")}`
				}
			})
			.then(res => res.json())
			.then(data => {

				setOrders(data.map(order => {
				return (
					<UserOrder key={order._id} 
						activeProductProp = {order}  
						eventHandler = {clickHandler}  
						changeHandler = {qtyHandler}
						addHandler = {addClickHandler}
						subHandler = {subtractClickHandler}
					/>
				)
			}))
		})
	}


	const qtyHandler = (e) => {
		console.log("This is the change" + e)
	}


	const addClickHandler = (e, qty) => {
		console.log("ADD" + e  + qty)
	}


	const subtractClickHandler = (e, qty) => {
		console.log("SUBTRACT" + e + qty)
	}


	const clickHandler = (e) => {
		Swal.fire({
		  title: 'Are you sure?',
		  text: "You won't be able to revert this!",
		  icon: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Yes, delete it!'
		}).then((result) => {
		  if (result.isConfirmed) {
		  	removeOrder(e)
		    Swal.fire(
		      'Deleted!',
		      'Your file has been deleted.',
		      'success'
		    )
		  }
		})


		//removeOrder(e)
	}

	const removeOrder = (e) => {
		fetch(`${process.env.REACT_APP_API_URL}/orders/${e}/remove`,{
			method: "DELETE",
			headers: {
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {})

		fetchProductData()
	}


	const totalPrice = () => {

	}


	useEffect(()=>{
		fetchProductData()
	}, [])

	return(
		(user.isAdmin === false)
		?
		<>
			<div className="mt-5 mb-3 text-center">
				<h1>Order Summary</h1>
			</div>
			<Table striped bordered hover variant="dark" id="OrderTable">
		     <thead>
		       <tr>
		         <th>Name</th>
		         <th>Model</th>
		         <th>Description</th>
		         <th>Price</th>
		         <th>Quantity</th>
		         <th>SubTotal</th>
		         <th>Action</th>
		       </tr>
		     </thead>
		     <tbody>
		       	{ orders }
		     </tbody>
		     <tbody>
		     	<tr>
		       		<td colSpan={3}>
		       		 <Button size="md" className="CheckoutBTN" onClick={()=> {
		       		 	let tblRow = document.getElementById("OrderTable").rows.length;

		       		 	if(tblRow > 2){
			       		 	Swal.fire({
								title: "Successful!",
								icon: "success",
								text: "Checkout successful. Thank you for your order."
			       		 	})

			       		 	navigate("/");
			       		 } else {
			       		 	Swal.fire({
								title: "Sorry",
								icon: "error",
								text: "No product to checkout."
							})
							navigate("/products");
			       		 }
		       		 }}
		       		 >
		       		 Proceed to Checkout</Button>
		       		</td>
		       		<td colSpan={4}>
		       		 <h3>Total Price: </h3>
		       		</td>
		       	</tr>
		     </tbody>
		   </Table>
		</>
		:
		<Navigate to="/products" />
	)
}