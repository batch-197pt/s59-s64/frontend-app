import { useState, useEffect, useContext } from 'react';
import { Navigate, Link, useNavigate } from 'react-router-dom';
import { Form, Button, Container, Row, Col } from 'react-bootstrap';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';


export default function NewProduct () {

	const {user} = useContext(UserContext);

	const navigate = useNavigate();

	const [name, setName] = useState('');
	const [model, setModel] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState(0);

	const [isActive, setIsActive] = useState(false);

	function addProduct(e) {
		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/products/`, {
	    	method: "POST",
	    	headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
			    name: name,
			    model: model,
			    description: description,
			    price: price,
			})
	    })
	    .then(res => res.json())
	    .then(data => {

	    	if(data){
	    		Swal.fire({
	    		    title: "Product Added",
	    		    icon: "success",
	    		    text: `${name} is now added`
	    		});

	    		navigate("/dashboard");
	    	}
	    	else{
	    		Swal.fire({
	    		    title: "Error!",
	    		    icon: "error",
	    		    text: `Something went wrong. Please try again later!`
	    		});
	    	}

	    })

	    // Clear input fields
	    setName("");
	    setModel("");
	    setDescription("");
	    setPrice(0);

	}

	useEffect(() => {

        if(name !== "" && model !== "" && description !== "" && price > 0){
            setIsActive(true);
        } else {
            setIsActive(false);
        }

    }, [name, model, description, price]);



	return (
		user.isAdmin
    	?
    	<Row className="NewProductRow">
			<Col md={6} sm={12}>
				<Form className="NewProductForm" onSubmit={(e) => addProduct(e)}>
					<h3 className="text-center">NEW PRODUCT</h3>

					  <Form.Group controlId="productName">
				        <Form.Label>Product Name</Form.Label>
				        <Form.Control
				        	type="text"
				        	placeholder="Enter product name"
				        	value = {name}
				        	onChange = {e => setName(e.target.value)} 
				        	required
				        />
				      </Form.Group>

				      <Form.Group controlId="productModel">
				        <Form.Label>Model</Form.Label>
				        <Form.Control
				        	type="text"
				        	placeholder="Enter product model"
				        	value = {model}
				        	onChange = {e => setModel(e.target.value)} 
				        	required
				        />
				      </Form.Group>

				      <Form.Group controlId="productDescription">
				        <Form.Label>Description</Form.Label>
				        <Form.Control
				        	type="text"
				        	placeholder="Enter product description"
				        	value = {description}
				        	onChange = {e => setDescription(e.target.value)} 
				        	required 
				        />
				      </Form.Group>

				      <Form.Group controlId="productPrice">
				        <Form.Label>Price</Form.Label>
				        <Form.Control
				        	type="number"
				        	value = {price}
				            onChange={e => setPrice(e.target.value)}
				            required

				        	/>
				      </Form.Group>
				    

				      { isActive 
					    	? 
					    	<Button className="NewProductBTN" variant="success" type="submit" id="submitBtn">
					    		Add
					    	</Button>
					        : 
					        <Button className="NewProductBTN" variant="primary" type="submit" id="submitBtn" disabled>
					        	Add
					        </Button>
					    }
					    	<Button className="NewProductBTN" as={Link} to="/dashboard" variant="danger" type="submit" id="submitBtn">
					    		Cancel
					    	</Button>
				</Form>
			</Col>
		</Row>
	    :
    	    <Navigate to="/products" />

	)
}