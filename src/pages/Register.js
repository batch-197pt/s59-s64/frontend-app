import { useState, useEffect, useContext } from 'react';
import { Form, Button, Container, Row, Col } from 'react-bootstrap';
import { Navigate, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';


export default function Register() {


	const { user }  = useContext(UserContext)

	const navigate = useNavigate();

	//State hooks to store the values of the input fields
	const [firstName, setFirstName] = useState("");
	const [lastName, setLastName] = useState("");
	const [email, setEmail] = useState("");
	const [mobileNo, setMobileNo] = useState("");
	const [password, setPassword] = useState("");
	const [verifyPassword, setverifyPassword] = useState("");


	const [isActive, setIsActive] = useState(false);


	function registerUser(e) {
		e.preventDefault()

		fetch(`${process.env.REACT_APP_API_URL}/users/registration`, {
			method: "POST",
			headers:{
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				email: email,
				mobileNo: mobileNo,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log('Success', data);

			if(data === true) {
				setFirstName("");
		        setLastName("");
		        setEmail("");
		        setMobileNo("");
		        setPassword("");
		        setverifyPassword("");

				Swal.fire({
					title: "Registration Successful",
					icon: "success",
					text: "You have successfully registered your account"
				})

		        navigate('/login')
			} else {

				// Clear form
		        setFirstName("");
		        setLastName("");
		        setEmail("");
		        setMobileNo("");
		        setPassword("");
		        setverifyPassword("");

				Swal.fire({
					title: "Email Already Exist!",
					icon: "error",
					text: "Email is already registered, please login to continue"
				})

				navigate('/login')
			}
		})

	}

	

	



	useEffect(() => {
		if((firstName !== "" && lastName !== "" && mobileNo.length === 11 && email !== ""&& password !== "" && verifyPassword !== "") && (password === verifyPassword)){

			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [firstName, lastName, email, mobileNo, password, verifyPassword])




	return (
		(user.id !== null) ? 
			<Navigate to="/login"/>
		:
		<Row className="RegisterRow">
			<Col md={6} sm={12}>
				<Form className="regForm" onSubmit={(e) => registerUser(e)}>

				<h3 className="formFieldHeader text-center">REGISTER</h3>

				  <Form.Group className="formField" controlId="fistName">
			        <Form.Label>First Name</Form.Label>
			        <Form.Control 
			        	type="text"
			        	value = {firstName}
			        	onChange={(e) => {setFirstName(e.target.value)}}
			        	placeholder="Enter your First Name" />
			      </Form.Group>

			      <Form.Group className="formField" controlId="lastName">
			        <Form.Label>Last Name</Form.Label>
			        <Form.Control 
			        	type="text"
			        	value = {lastName}
			        	onChange={(e) => {setLastName(e.target.value)}}
			        	placeholder="Enter your Last Name" />
			      </Form.Group>

			      <Form.Group className="formField" controlId="userEmail">
			        <Form.Label>Email address</Form.Label>
			        <Form.Control 
			        	type="email"
			        	value = {email}
			        	onChange={(e) => {setEmail(e.target.value)}}
			        	placeholder="Enter your email" />
			      </Form.Group>

			      <Form.Group className="formField" controlId="mobileNo">
			        <Form.Label>Mobile Number</Form.Label>
			        <Form.Control 
			        	type="text" 
			        	value = {mobileNo}
			        	onChange={(e) => {setMobileNo(e.target.value)}}
			        	placeholder="09XXXXXXXXX" />
			      </Form.Group>

			      <Form.Group className="formField" controlId="password">
			        <Form.Label>Password</Form.Label>
			        <Form.Control 
			        	type="password"
			        	value = {password}
			        	onChange={(e) => {setPassword(e.target.value)}}
			        	placeholder="Password" />
			      </Form.Group>

			      <Form.Group className="formField" controlId="verifyPassword">
			        <Form.Label>Verify Password</Form.Label>
			        <Form.Control
			        	type="password"
			        	value = {verifyPassword}
			        	onChange={(e) => {setverifyPassword(e.target.value)}}
			        	placeholder="Password" />
			      </Form.Group>

			      { isActive ? 
			      		<Button className="formField formBTN" type="submit" id="submitBtn">
			        		Submit
			      		</Button>
			      		:
			      		<Button className="formField formBTN" type="submit" id="submitBtn" disabled>
			        		Submit
			      		</Button>

			      }
			      
			      
			    </Form>
		    </Col>
		</Row>			
	)


}