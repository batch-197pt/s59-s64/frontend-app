import { Fragment, useEffect, useState, useContext } from 'react';
import { Accordion, Table, ButtonGroup, ButtonToolbar, Button, Container, Card  } from 'react-bootstrap';
import { Navigate, Link } from 'react-router-dom';
import Swal from "sweetalert2";
import UserContext from "../UserContext";



export default function AdminDashboard(){

	const {user} = useContext(UserContext);
	

	const [allProducts, setAllProducts] = useState([]);
	const [allUsers, setAllUsers] = useState([]);

	const fetchData = () =>{

		fetch(`${process.env.REACT_APP_API_URL}/products/all`,{
			headers:{
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {

			setAllProducts(data.map(product => {
				return(
					<tr key={product._id}>

						{/*<td>{product._id}</td>*/}
						<td>{product.name}</td>
						<td>{product.model}</td>
						<td>{product.description}</td>
						<td>{product.price}</td>
						<td>{product.isActive ? "Active" : "Inactive"}</td>
						<td><Button className="AdminBTN" as={ Link } to={`/edit-products/${product._id}`} variant="secondary" size="sm">Edit</Button>
						</td>
						<td>
							{
								(product.isActive)
								?	
									<Button className="AdminBTN" variant="danger" size="sm" onClick ={() => archive(product._id, product.name)}>Archive</Button>
								:
										<Button className="AdminBTN" variant="success" size="sm" onClick ={() => reactivate(product._id, product.name)}>Reactivate</Button>
									
							}
						</td>
					</tr>
				)
			}))

		})
	}

	//Making the product inactive
	const archive = (productId, productName) =>{

		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/archive`,{
			method: "PUT",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				isActive: false
			})
		})
		.then(res => res.json())
		.then(data =>{

			if(data){
				Swal.fire({
					title: "Deactivation Succesful!",
					icon: "success",
					text: `${productName} is now inactive.`
				})
				fetchData();
			}
			else{
				Swal.fire({
					title: "Deactivation Unsuccessful!",
					icon: "error",
					text: `Something went wrong. Please try again later!`
				})
			}
		})
	}

	//Making the product active
	const reactivate = (productId, productName) =>{

		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/archive`,{
			method: "PUT",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				isActive: true
			})
		})
		.then(res => res.json())
		.then(data =>{

			if(data){
				Swal.fire({
					title: "Reactivation Succesful!",
					icon: "success",
					text: `${productName} is now active.`
				})
				fetchData();
			}
			else{
				Swal.fire({
					title: "Reactivation Unsuccessful!",
					icon: "error",
					text: `Something went wrong. Please try again later!`
				})
			}
		})
	}

	const fetchUserData = () => {

		fetch(`${process.env.REACT_APP_API_URL}/users/`,{
			headers:{
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {


			setAllUsers(data.map(user => {
				return(
					<tr key={user._id}>

						<td>{user.firstName}</td>
						<td>{user.lastName}</td>
						<td>{user.email}</td>
						<td>{user.mobileNo}</td>
						<td>{user.isAdmin ? "Admin" : "Not Admin"}</td>
						<td>
							{
								(user.isAdmin)
								?	
									<Button className="AdminBTN" variant="danger" size="sm" onClick ={() => removeAdmin(user._id, user.email)}>Remove Admin Permission</Button>
								:
									<Button className="AdminBTN" variant="success" size="sm" onClick ={() => setAdmin(user._id, user.email)}>Set As Admin</Button>
									
							}
						</td>
					</tr>
				)
			}))

		})
	}



	const removeAdmin = (userId, userEmail) =>{

		fetch(`${process.env.REACT_APP_API_URL}/users/${userId}`,{
			method: "PUT",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				isAdmin: false
			})
		})
		.then(res => res.json())
		.then(data =>{


			if(data){
				Swal.fire({
					title: "Succesful!",
					icon: "success",
					text: `${userEmail}'s Admin permission removed.`
				})
				fetchUserData();
			}
			else{
				Swal.fire({
					title: "Unsuccessful!",
					icon: "error",
					text: `Something went wrong. Please try again later!`
				})
			}
		})
	}

	const setAdmin = (userId, userEmail) =>{

		fetch(`${process.env.REACT_APP_API_URL}/users/${userId}`,{
			method: "PUT",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				isAdmin: true
			})
		})
		.then(res => res.json())
		.then(data =>{


			if(data){
				Swal.fire({
					title: "Succesful!",
					icon: "success",
					text: `${userEmail} is now an Admin.`
				})
				fetchUserData();
			}
			else{
				Swal.fire({
					title: "Unsuccessful!",
					icon: "error",
					text: `Something went wrong. Please try again later!`
				})
			}
		})
	}


	// To fetch all product in the first render of the page.
	useEffect(()=>{
		// invoke fetchData() to get all product.
		fetchData();
		fetchUserData();
	}, [])

	return(
		(user.isAdmin)
		?
		<>
			<div className="mt-5 mb-3 text-center">
				<h3>ADMIN DASHBOARD</h3>
			</div>
			<Accordion defaultActiveKey="0">
		      <Accordion.Item eventKey="0">
		        <Accordion.Header>PRODUCTS
		        <Button as={Link} to="/products/newProduct" variant="primary" size="sm" className="mx-2">Add Product</Button>
		        </Accordion.Header>
		        <Accordion.Body className="AdminAccord">
		          <h3>PRODUCTS LIST</h3>
		          <Table striped bordered hover variant="dark" responsive>
				     <thead className="ProductsTable text-center">
				       <tr>
				         {/*<th>Product ID</th>*/}
				         <th>NAME</th>
				         <th>MODEL</th>
				         <th>DESCRIPTION</th>
				         <th>PRICE</th>
				         <th>STATUS</th>
				         <th>EDIT PRODUCT</th>
				         <th>ACTION</th>
				       </tr>
				     </thead>
				     <tbody>
				       { allProducts }
				     </tbody>
				   </Table>

		        </Accordion.Body>
		      </Accordion.Item>
		      <Accordion.Item eventKey="1" >
		        <Accordion.Header>USERS</Accordion.Header>
		        <Accordion.Body className="AdminAccord">
		          	<h3>USERS LIST</h3>
		        	<Table striped bordered hover variant="dark" responsive>
				     <thead>
				       <tr>
				         <th>First Name</th>
				         <th>Last Name</th>
				         <th>Email Address</th>
				         <th>Mobile No.</th>
				         <th>Status</th>
				         <th>Set to Admin?</th>
				       </tr>
				     </thead>
				     <tbody>
				       { allUsers }
				     </tbody>
				   </Table>

		        </Accordion.Body>
		      </Accordion.Item>
		    </Accordion>
			
		</>
		:
		<Navigate to="/products" />
	)
}