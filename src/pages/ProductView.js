import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

import productViewImage from '../images/sampleProd.png'
import '../styleSheets/ProductView.css'


export default function ProductView() {

	const { user } = useContext(UserContext)

	
	const navigate = useNavigate();

	const { productId } = useParams();

	const [name, setName] = useState("");
	const [model, setModel] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	// const [quantity, setQuantity] = useState(1);


	const buyProduct = (productId) => {

		fetch(`${process.env.REACT_APP_API_URL}/orders/add`, {
			method: "POST",
			headers: {
				'Content-Type': "application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				productId: productId,
				//quantity: quantity
			})
		})
		.then(res => res.json())
		.then(data => {


			if(data === true) {
				Swal.fire({
				title: "Successfully Added!",
				icon: "success",
				text: "You have successfully added this product."
			})

			navigate("/products");

		} else {

				Swal.fire({
				title: "Something went wrong",
				icon: "error",
				text: "Please try again."
			})
		}

	})

};


	useEffect(() => {

		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(res => res.json())
		.then(data => {

			setName(data.name);
			setModel(data.model);
			setDescription(data.description);
			setPrice(data.price);
		})

	}, [productId])

	return (

		<Container bg="dark">
			<Row className="productViewRow my-auto">
			  <Col xs={12} md={6}>
			  	<img className="productViewImage" src={productViewImage} alt="gamingPC" />
			  </Col>
			  <Col xs={12} md={6}>
			    <Card className="productViewCard my-3" bg="dark">
			        <Card.Body>
			          <Card.Title className="text-center my-3">{name}</Card.Title>
			          <Card.Subtitle>Model:</Card.Subtitle>
			          <Card.Text>{model}</Card.Text>
			          <Card.Subtitle>Description:</Card.Subtitle>
			          <Card.Text>{description}</Card.Text>
			          <Card.Subtitle>Price:</Card.Subtitle>
			          <Card.Text>{price}</Card.Text>
			          <Card.Subtitle>Quantity:</Card.Subtitle>

			          <div className="d-grip gap-2 my-2">
			          {
			          		(user.id !== null) ?
			          			<>
			          			<Button className="productViewBTN" onClick={() => buyProduct(productId)}>Add to Order</Button>
			          			<Button className="productViewBTN" as={Link} to="/products">Return to Products</Button>
			          			</>
			          			:
			          			<Button className="productViewBTN" as={Link} to="/login">Login</Button>
			          }
			          </div>
			          </Card.Body>
			      </Card>
			  </Col>
			</Row>
		</Container>

	)

}
