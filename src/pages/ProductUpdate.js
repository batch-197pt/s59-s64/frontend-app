import { useState, useEffect, useContext } from 'react';
import { Navigate, Link, useParams, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';


import { Form, Button, Row, Col } from 'react-bootstrap';


export default function ProductUpdate() {

	const {user} = useContext(UserContext);

	const { productId } = useParams();

	const navigate = useNavigate();

	// State hooks to store the values of the input fields
	const [name, setName] = useState("");
	const [model, setModel] = useState("")
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);


    // State to determine whether submit button is enabled or not
    const [isActive, setIsActive] = useState(false);

	
	function updateProduct(e) {

	    e.preventDefault();

	    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`, {
	    	method: "PUT",
	    	headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
			    name: name,
			    model: model,
			    description: description,
			    price: price,
			})
	    })
	    .then(res => res.json())
	    .then(data => {

	    	if(data){
	    		Swal.fire({
	    		    title: "Product succesfully Updated",
	    		    icon: "success",
	    		    text: `${name} is now updated`
	    		});

	    		navigate("/dashboard");
	    	}
	    	else{
	    		Swal.fire({
	    		    title: "Update Error!",
	    		    icon: "error",
	    		    text: `Something went wrong. Please try again later!`
	    		});
	    	}

	    })

	    // Clear input fields
	    setName("");
	    setModel("");
	    setDescription("");
	    setPrice(0);
	}

	useEffect(() => {

        // Validation to enable submit button when all fields are populated and set a price and slot greater than zero.
        if(name !== "" && model !== "" && description !== "" && price > 0 ){
            setIsActive(true);
        } else {
            setIsActive(false);
        }

    }, [name, model, description, price]);


	//To get the information of the course to be updated.
    useEffect(()=> {


    	fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
    	.then(res => res.json())
    	.then(data => {


    		setName(data.name);
    		setModel(data.model);
    		setDescription(data.description);
    		setPrice(data.price);


    	});

    }, [productId]);

    return (
    	user.isAdmin
    	?
			<Row className="UpdateProductRow">
				<Col md={6} sm={12}>
		    	
			        <Form className="UpdateProductForm" onSubmit={(e) => updateProduct(e)}>
			        	<h3 className="text-center">UPDATE PRODUCT</h3>

			        	<Form.Group controlId="productName" className="mb-3">
			                <Form.Label>Name</Form.Label>
			                <Form.Control 
				                type="text" 
				                placeholder="Enter Product Name" 
				                value = {name}
				                onChange={e => setName(e.target.value)}
				                required
			                />
			            </Form.Group>

			            <Form.Group controlId="productModel" className="mb-3">
			                <Form.Label>Model</Form.Label>
			                <Form.Control 
				                type="text" 
				                placeholder="Enter Product Model" 
				                value = {model}
				                onChange={e => setModel(e.target.value)}
				                required
			                />
			            </Form.Group>

			            <Form.Group controlId="productDescription" className="mb-3">
			                <Form.Label>Description</Form.Label>
			                <Form.Control
			                	as="textarea"
			                	rows={2}
				                placeholder="Enter Product Description" 
				                value = {description}
				                onChange={e => setDescription(e.target.value)}
				                required
			                />
			            </Form.Group>

			            <Form.Group controlId="productPrice" className="mb-3">
			                <Form.Label>Price</Form.Label>
			                <Form.Control 
				                type="number" 
				                placeholder="Enter Product Price" 
				                value = {price}
				                onChange={e => setPrice(e.target.value)}
				                required
			                />
			            </Form.Group>

			            
		        	    { isActive 
		        	    	? 
		        	    	<Button className="UpdateProductBTN" variant="success" type="submit" id="submitBtn">
		        	    		Update
		        	    	</Button>
		        	        : 
		        	        <Button className="UpdateProductBTN" variant="Primary" type="submit" id="submitBtn" disabled>
		        	        	Update
		        	        </Button>
		        	    }
		        	    	<Button className="UpdateProductBTN" as={Link} to="/dashboard" variant="danger" type="submit" id="submitBtn">
		        	    		Cancel
		        	    	</Button>
			        </Form>
			    </Col>
			</Row>
	    
    	:
    	    <Navigate to="/products" />
	    	
    )

}
