import { Fragment, useEffect, useState } from 'react';
import ProductCard from '../components/ProductCard';
//import productsData from '../data/productsData';

import '../index.css';

export default function Products() {

	//console.log(productsData)
	const [products, setProducts] = useState([]);


	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/`)
		.then(res => res.json())
		.then(data => {

			setProducts(data.map(product => {
				return (
					<ProductCard key={product._id} productProp = {product} />
				)
			}))

		})
	}, [])

	return (
		<Fragment>
			<h3 className="mt-5 mb-3 text-center">Products</h3>
			<div className="productField">
					{products}
			</div>
		</Fragment>

	)
}
