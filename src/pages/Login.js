import { useState, useEffect, useContext } from 'react';
import { Form, Button, Row, Col } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';




export default function Login() {


	const { user, setUser } = useContext(UserContext);

	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");

	const [isActive, setIsActive] = useState(false);



	function loginUser (e) {

		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
			method: "POST",
			headers:{
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {

			if(typeof data.access !== "undefined"){
				localStorage.setItem('token', data.access);
				retrieveUserDetails(data.access)

				Swal.fire({
					title: "Sucessful",
					icon: "success",
					text: "You have successfully logged in."
				})
			} else {

				Swal.fire({
					title: "Authentication Failed",
					icon: "error",
					text: "Please check your login details and try again"
				})
			}

		})


		setEmail("");
		setPassword("");
	}


	const decodeToken = (token) => {
    	return JSON.parse(atob(token.split('.')[1]));
	}

	const retrieveUserDetails = (token) => {
		
		const userData = decodeToken(token)


		fetch(`${process.env.REACT_APP_API_URL}/users/${userData.id}`, {
			headers: {
				Authorization: `Bearer ${ token }`
			}
		})
		.then(res => res.json())
		.then((data) => {
			
			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			})
		})
	}

	useEffect(() => {
		if(email !== "" && password !== "" )  {

			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [email, password])


	return (
		(user.id !== null && user.isAdmin) ?
			<Navigate to="/dashboard"/>
		:
		(user.id !== null && user.isAdmin == false) ?
			<Navigate to="/products"/>
		:
		<Row className="LoginRow">
			<Col md={6} sm={12}>
				<Form className="LoginForm" onSubmit={(e) => loginUser(e)}>
				<h3 className="text-center">LOGIN</h3>

			      <Form.Group className="mb-3 " controlId="userEmail">
			        <Form.Label>Email address</Form.Label>
			        <Form.Control
			        		type="email"
				        	value={email}
				        	onChange={(e) => {setEmail(e.target.value)}}
							placeholder="Enter your email" />
			      </Form.Group>

			      <Form.Group className="mb-3" controlId="password">
			        <Form.Label>Password</Form.Label>
			        <Form.Control 
			        		type="password"
			        		value={password}
				        	onChange={(e) => {setPassword(e.target.value)}}
				        	placeholder="Password" />
			      </Form.Group>

			      { isActive ?
			      			<Button className="LoginBTN" variant="primary" type="submit" id="submitBtn">
			      		 	 Login
			      			</Button>
			      			:
			      			<Button className="LoginBTN" variant="danger" type="submit" id="submitBtn" disabled>
			      			  Login
			      			</Button>
				  }

			    </Form>
	    	</Col>
		</Row>	
	)


}