import { Fragment } from 'react';
import { Row, Col, Carousel } from 'react-bootstrap'
import homeImage from '../images/homeIMG.png'
import homeImage2 from '../images/homeIMG2.png'
import homeImage3 from '../images/homeIMG3.png'

//import '../App.css';
// import '../index.css';

export default function	Home() {

	return(
		<Fragment>
		<Row>
			<Col sm={12}>
				<Carousel>
		      		<Carousel.Item interval={5000}>
						<img className="carouselImage" src={homeImage} alt="gamingPC" />
		      		</Carousel.Item>
		      		<Carousel.Item interval={5000}>
						<img className="carouselImage" src={homeImage2} alt="gamingPC" />
		      		</Carousel.Item>
		      		<Carousel.Item interval={5000}>
						<img className="carouselImage" src={homeImage3} alt="gamingPC" />
		      		</Carousel.Item>
		      	</Carousel>
	      	</Col>
	     </Row>
		</Fragment>
	)


 
}