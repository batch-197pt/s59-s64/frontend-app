const productsData = [

	{
		id: 1,
		name: "ASUS Vivo Book",
		model: "M3401QA-KM050WS",
		description: "14.0 OLED Display; Ryzen 5 5600H Processor; 512GB SSD Storage and 8GB DDR4 Memory",
		price: 68500,
		isActive: true
	}, 
	{
		id: 2,
		name: "Lenovo Legion 5",
		model: "15ACH6H",
		description: "15.6 WQHD Display; Ryzen 7 5800H Processor; 512GB SSD Storage and 16GB DDR4 Memory",
		price: 77500,
		isActive: false
	}



];

export default productsData;