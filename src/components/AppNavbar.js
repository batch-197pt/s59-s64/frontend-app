import { useContext } from 'react';
import { Navbar, Nav, NavDropdown, Container } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import UserContext from '../UserContext';


import './AppNav.css'


export default function AppNavbar(){

	const { user } = useContext(UserContext);

	return (
		<Navbar expand="lg" className="appNavbar" bg="dark" variant="dark">
	      <Container fluid>
	        <Navbar.Brand className="webName" as={Link} to="/">GamePlay</Navbar.Brand>
	        <Navbar.Toggle aria-controls="navbarScroll" />
	        <Navbar.Collapse className="SplitMenu" id="navbarScroll">
	          <Nav className="ms-auto">
	          	  <Nav.Link className="navLink" as={Link} to="/">Home</Nav.Link>

	          	   
		          { (user.id !== null) ?
		          		<>
		          			{
				          	  	(user.isAdmin)
				          	  	?
				          	  	<Nav.Link className="navLink" as={Link} to="/dashboard" >Dashboard</Nav.Link>
				          	  	:
				          	  	<>
				          	  	<Nav.Link className="navLink" as={Link} to="/products">Products</Nav.Link>
				          	  	<Nav.Link className="navLink" as={Link} to="/orders">Orders</Nav.Link>
				          	  	</>
	          	  			}
		          			<Nav.Link className="navLink" as={Link} to="/logout" >Logout</Nav.Link>
		          		</>
		          		:
		          		<>
		          			<Nav.Link className="navLink" as={Link} to="/register">Register</Nav.Link>
		          			<Nav.Link className="navLink" as={Link} to="/login" >Login</Nav.Link>
		          		</>
		          }

		          	

	          </Nav>
	        </Navbar.Collapse>
	      </Container>
	    </Navbar>

	)
}