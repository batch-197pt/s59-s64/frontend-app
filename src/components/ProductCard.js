import { Row, Col, Card, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import prodImage from '../images/productImage.png'


import "./prodCard.css"


export default function ProductCard({productProp}) {

	  //console.log({productProp});
	  

	  const { name, model, price, _id } = productProp;

	  return (
	  	<Row className="cardRow my-3">
	          <Col className="cardColumn" lg={12}>
	              <Card className="productCard my-3" bg="dark" >
	              	  <Card.Img variant="top" src={prodImage} />

	                  <Card.Body className="cardBody">
	                      <Card.Title className="cardDetails mb-4">{name}</Card.Title>
	                      <Card.Subtitle className="cardSubTitle">Model:</Card.Subtitle>
	                      <Card.Text className="cardDetails">{model}</Card.Text>
	                      <Card.Subtitle className="cardSubTitle">Price:</Card.Subtitle>
	                      <Card.Text className="cardDetails">{price}</Card.Text>
	                      <Button variant="primary" as={Link} to={`/products/${_id}`}>View Details</Button>
	                  </Card.Body>
	              </Card>
	          </Col>
	      </Row>
	  )
	}

