import { Fragment, useEffect, useState, useContext } from 'react';
import { ButtonGroup, ButtonToolbar, Button, Container, Form, InputGroup, Table } from 'react-bootstrap';
import { Navigate, Link } from 'react-router-dom';
import Swal from "sweetalert2";
import UserContext from "../UserContext";


export default function UserOrder(props){

	let prodId = props.activeProductProp.products[0].productId
	let prodQty = props.activeProductProp.products[0].quantity

	const {user} = useContext(UserContext);

	const { _id, userId, products, totalAmount } = props.activeProductProp;

	const [orderQty, setOrderQty] = useState(0)
	const [orders, setOrders] = useState([]);

	const fetchOrderData = () => {
		fetch(`${process.env.REACT_APP_API_URL}/products/`)
		.then(res => res.json())
		.then(data => {
				for(let i=0; i < data.length; i++){
					if(prodId == data[i]._id){
						return (
							setOrders(	
								<tr key={data[i]._id}>
									<td>{data[i].name}</td>
									<td>{data[i].model}</td>
									<td>{data[i].description}</td>
									<td>{data[i].price}</td>
									<td>{prodQty}
										{/*<InputGroup className="mb-3">
								        	<Button 
								        		variant="outline-secondary" 
								        		id="subtractBTN" 
								        		onClick ={() => {
								        			setOrderQty({prodQty})
												props.subHandler(prodQty, orderQty)
												}}
											>
									          -
									        </Button>
								        	<Form.Control
									          value={orderQty}
									          onChange = {() => {
									          	setOrderQty({prodQty})
													props.changeHandler({orderQty})
												}}
								        	/>
								        	<Button
								        		variant="outline-secondary" 
								        		id="addBTN"
								        		onClick ={() => {
												props.addHandler(props.activeProductProp.products[0].quantity, orderQty)
												}}
								        	>
									          +
									        </Button>
								        </InputGroup>*/}
									</td>
									<td>{totalAmount}</td>
									<td>
										<Button variant="danger" size="sm" onClick ={() => {
											
											props.eventHandler(props.activeProductProp._id)
										}}>
										Remove</Button>
									</td>
								</tr>
							)
						)
					} else {
						
					}
				}
		})


	}





	useEffect(()=>{
		fetchOrderData();
	}, [])

	return (
		<>
	       { orders }
	     </>

	)
}